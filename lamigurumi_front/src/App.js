import React from 'react';

import { Route } from 'react-router-dom';

import HomePage from './pages/HomePage';
import ProductsPage from './pages/ProductsPage';
import AboutPage from './pages/AboutPage';
import {Col, Container, Row} from "shards-react";
import logo from "./assets/logo.jpg";
import LgNavbar from "./components/LgNavbar";
import LgFooter from "./components/LgFooter";


function App() {
    return (
        <Container>
            <Row>
                <Col className="d-flex justify-content-center">
                    <img alt="LogoLamigurumi" src={logo} height={200}  />
                </Col>
            </Row>
            <Row>
                <Col>
                    <LgNavbar/>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/products" component={ProductsPage}/>
                    <Route path="/about" component={AboutPage}/>
                </Col>
            </Row>
            <Row>
                <Col>
                    <LgFooter/>
                </Col>
            </Row>
        </Container>
    )
}

export default App;
